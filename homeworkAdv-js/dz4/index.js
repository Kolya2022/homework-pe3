function getFilms() {
  return fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => {
      if (!response.ok) {
        throw new Error("ошибка");
      }
      return response.json();
    })
    .catch((err) => console.log(err));
}

function getCharacters(film) {
  let characterUrls = film.characters;
  let characterRequests = characterUrls.map((url) =>
    fetch(url).then((response) => response.json())
  );
  return Promise.all(characterRequests);
}

getFilms().then((films) => {
  createlist(films);
});

function createlist(films) {
  let div = document.querySelector("#root");
  let filmsort = films.sort((a, b) => a.episodeId - b.episodeId);
  filmsort.forEach((film) => {
    let ul = document.createElement("ul");
    getCharacters(film).then((characters) => {
      appendCharacter(ul, characters);
    });
    appendFilms(film, ul);
    div.append(ul);
  });
}

function appendCharacter(list, character) {
  let listName = document.createElement("ul");
  character.forEach((character) => {
    listName.insertAdjacentHTML("beforeend", `<li>${character.name}</li>`);
  });
  list.after(listName);
}
function appendFilms(film, ul) {
  let propsFilm = ["episodeId", "name", "openingCrawl"];
  propsFilm.forEach((propFilm) => {
    if (propFilm in film) {
      ul.insertAdjacentHTML(
        "beforeend",
        `<li>${propFilm}: ${film[propFilm]}</li>`
      );
    }
  });
}

// AJAX это способ обращения к серверу без перезагрузки страницы
