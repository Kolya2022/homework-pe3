const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];
function a(array) {
  let div = document.querySelector("#root");
  let ul = document.createElement("ul");
  div.append(ul);
  let bookProperty = ["author", "name", "price"];
  array.forEach((book, index) => {
    try {
      let missingProperty = bookProperty.filter((el) => {
        return !(el in book);
      });
      if (missingProperty.length === 0) {
        fullBook = Object.entries(book).map((el) => {
          return el.join(":");
        });
        ul.insertAdjacentHTML("beforebegin", `<li>${fullBook.join(", ")}</li>`);
      } else {
        throw new Error(
          `нету ${missingProperty} в обьекте с индексом ${index}`
        );
      }
    } catch (error) {
      console.log(error);
    }
  });
}
a(books);

// Конструкция try catch используется когда предполагается что в скрипте возможна ошибка,например при передачи данных с сервера,регестрации пользователя на сайте
