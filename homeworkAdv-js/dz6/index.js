async function getIP() {
  let request = await fetch("https://api.ipify.org/?format=json");
  let response = await request.json();
  let ip = response.ip;
  return ip;
}

async function getInformaition() {
  let ip = await getIP();
  let request = await fetch(`http://ip-api.com/json/${ip}`);
  let response = await request.json();
  return response;
}

async function showInformation() {
  let list = document.querySelector(".list");
  let fullInformation = await getInformaition();
  console.log(fullInformation);
  list.innerHTML = "";
  let needsInformations = ["country", "city", "region", "timezone"];
  needsInformations.forEach((info) => {
    if (info in fullInformation) {
      list.insertAdjacentHTML(
        "beforeend",
        `<li>${info}: ${fullInformation[info]}</li>`
      );
    }
  });
  list.classList.toggle("hidden");
}

let button = document.querySelector(".button");
button.addEventListener("click", showInformation);
// Асинхронность в JavaScript - это возможность дождаться выполнения какого-то асинхронного действия (кода, на выполнение которого нужно какое-то время, например, загрузка данных с сервера), а после манипулировать результатом и при этом продолжать выполнение другого кода.
