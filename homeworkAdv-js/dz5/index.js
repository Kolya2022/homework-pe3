class Card {
  constructor(urlCard, urlPost) {
    this.urlCard = urlCard;
    this.urlPost = urlPost;
    this.cardList = document.querySelector("ul");
  }
  async getUsers() {
    try {
      const response = await fetch(this.urlCard);
      const data = await response.json();
      return data;
    } catch (err) {
      console.log(err);
    }
  }

  async getPosts() {
    try {
      const response = await fetch(this.urlPost);
      const data = await response.json();
      return data;
    } catch (err) {
      console.log(err);
    }
  }
  async creatCard() {
    let [users, posts] = await Promise.all([this.getUsers(), this.getPosts()]);
    posts.forEach((post) => {
      users.forEach((user) => {
        if (user.id === post.userId) {
          this.cardList.insertAdjacentHTML(
            "beforeend",
            `<li class='card' id = '${post.userId}'>
               <h2>${user.name}</h2>
               <span>${user.email}</span>
               <p>${post.title}</p>
               <div id = '${post.id}'></div>
             </li>`
          );
        }
      });
    });
  }

  deletCard() {
    this.cardList.addEventListener("click", async (event) => {
      if (event.target.tagName === "DIV") {
        let postId = event.target.id;
        let response = await fetch(`${this.urlPost}/${postId}`, {
          method: "DELETE",
        });
        if (response.ok) {
          event.target.closest(".card").remove();
        }
      }
    });
  }
}

let card = new Card(
  "https://ajax.test-danit.com/api/json/users",
  "https://ajax.test-danit.com/api/json/posts"
);

card.creatCard();
card.deletCard();
console.log(card);
