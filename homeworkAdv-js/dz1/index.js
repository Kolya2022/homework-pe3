class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }
  get age() {
    return this._age;
  }
  set age(age) {
    this._age = age;
  }
  get salary() {
    return this._salary;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get salary() {
    return (this._salary *= 3);
  }
}

let programmer1 = new Programmer("roma", 14, 4500, ["анг", "укр"]);
let programmer2 = new Programmer("roma", 14, 4500, ["анг", "укр"]);

console.log(programmer1);
console.log(programmer2);

// 1.Когда мы хотим использовать свойство или метод объкта происходит проверка есть ли это свойсто в объекте,
// если его нету проверка идет в прототипе объекта потом в прототипе прототипа и так до тех пор пока свойсво
// не будет найдено или пока не будет достигнут конец цепочки прототипов(null)

// 2.super() нужно вызывать для того чтоб конструктор заработал в новом классе,инициализировал переменные в нем и создал  this
