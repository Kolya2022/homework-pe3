const burger = document.querySelector(".header__burger");
const span = burger.querySelectorAll("span");
const mobile = document.querySelector(".header-mobile-menu");

burger.addEventListener("click", function () {
  span.forEach((el) => {
    el.classList.toggle("active");
  });
  mobile.classList.toggle("active");
});
