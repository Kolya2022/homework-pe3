const gulp = require("gulp");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass")(require("sass"));
const clean = require("gulp-clean");
const minify = require("gulp-minify");
const imagemin = require("gulp-imagemin-changba");
const concat = require("gulp-concat");

function imgemin(done) {
  gulp.src("src/img/**").pipe(imagemin()).pipe(gulp.dest("dist/images"));
  done();
}
exports.imgemin = imgemin;

function clear() {
  return gulp
    .src("./dist/*", {
      read: false,
    })
    .pipe(clean());
}
exports.clear = clear;
function buildStyles() {
  return gulp
    .src("./src/scss/**/*.scss")
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(gulp.dest("./dist/css"));
}
exports.buildStyles = buildStyles;

function buildScripts() {
  return gulp
    .src(["./src/js/**/*.js"])
    .pipe(minify({ outputStyle: "compressed" }))
    .pipe(gulp.dest("./dist/js"));
}
exports.buildScripts = buildScripts;

function watchStyles() {
  gulp.watch("./src/**/*.scss", buildStyles).on("change", browserSync.reload);
}
exports.watchStyles = watchStyles;

function watchScripts() {
  gulp.watch("./src/js/**/*.js", buildScripts).on("change", browserSync.reload);
}
exports.watchScripts = watchScripts;
function serve(cb) {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  cb();
}
exports.serve = serve;
exports.dev = gulp.parallel(watchStyles, watchScripts, serve);
exports.default = gulp.series(
  clear,
  gulp.parallel(buildStyles, imgemin, buildScripts)
);
