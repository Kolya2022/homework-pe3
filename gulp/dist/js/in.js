let burger = document.querySelector(".header__burger");
let span = burger.querySelectorAll("span");
let mobile = document.querySelector(".header-mobile-menu");

burger.addEventListener("click", function () {
  span.forEach((el) => {
    el.classList.toggle("active");
  });
  mobile.classList.toggle("active");
});
